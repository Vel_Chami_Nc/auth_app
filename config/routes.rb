Rails.application.routes.draw do

  resources :todos
  root 'welcome#index'
  devise_for :users
  resources :users
  resources :todos
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
