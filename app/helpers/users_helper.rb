module UsersHelper
  def email(user)
    user.email 
  end 
  def attachment(user)
    image_tag user.avatar.variant(resize:'100x100') if user.avatar.attached?
  end
end
