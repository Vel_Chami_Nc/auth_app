class UsersController < ApplicationController
  before_action :set_user, only: [:show,:edit,:update,:destroy]
  load_and_authorize_resource
  def index
    @users = User.all
  end
 
  def new
    @user = User.new
  end 

  def create
    @user = User.create(user_params)
    if @user.save!
      redirect_to 'users_path', notice: "Successfully created"
    end 
  end 

  def show
    @user.add_role(params[:user][:role])
  end

  def edit
  end 

  def update 
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.'
    else 
      render :edit
    end 
  end 

  def destroy
    @user.destroy
    redirect_to 'users_path', notice: 'succesfully deleted'
  end 

  private

  def set_user 
    @user = User.find(params[:id])
  end 
 
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name)
  end 

end
